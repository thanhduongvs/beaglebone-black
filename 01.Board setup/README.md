# Setting up serial communication with the board

1. Trên ubuntu gõ lệnh `sudo apt-get install screen` để giao tiếp *uart* với board *beagle bone*.  
2. Sau khi đã cắm *UART* vào ubuntu gõ lệnh `ls /dev/ttyUSB*` để xem ubuntu có nhận uart không. Nếu nhận sẽ hiện ttyUSB0.  
3. Hoặc để xem thông tin của uart gõ lệnh `dmesg|tail`.  
4. Để kết nối với uart gõ lệnh `screen /dev/ttyUSB0 115200`.Với 115200 là baudrate.  
5. Trong trường hợp không thể kết nối với uart gõ lệnh `sudo chmod 777 /dev/ttyUSB0` rồi gõ lại lệnh `screen /dev/ttyUSB0 115200`.  
6. Chân TX và RX của Beaglebone Black như hình bên dưới.  
![Beaglebone Black](https://gitlab.com/thanhduongvs/beaglebone-black/raw/master/01.Board%20setup/image/01.beaglebone-black-serial.jpg)

# Bootloader interaction

7. Sau khi kết nối uart như các bước trên. Trên Beaglebone ,hãy nhấn nút **reset**. Rồi nhấn phím **Enter** trên bàn phím để vào **U-Boot** trên beaglebone.  
8. Kết quả như hình bên dưới:  
![U-Boot](https://gitlab.com/thanhduongvs/beaglebone-black/raw/master/01.Board%20setup/image/02.u-boot.png)
9. Để set lại biến môi trường gõ lệnh `env default -f -a` .  
10. Để save lại gõ lệnh `saveenv` .  
11. Để set ip tĩnh cho beagle gõ lệnh `setenv ipaddr 192.168.0.10` .  
12. Gõ lệnh `setenv serverip 192.168.0.11` . Đây là địa chỉ ip tĩnh của ubuntu sẽ cài đặt bên dưới.  
13. Để save lại gõ lệnh `saveenv` .  
14. Để thoát ra khỏi *screen* nhấn tổ hợp phím *Ctr + A + D*.  

# Setting up Ethernet communication

15. Cài đặt ip tĩnh cho ubuntu làm theo các bước bên dưới.  
16. Gõ lệnh `ifconfig` để xem card mạng máy tính ubuntu. Như hình bên dưới tên card mạng của mình là **enp0s25**.  
![ifconfig](https://gitlab.com/thanhduongvs/beaglebone-black/raw/master/01.Board%20setup/image/03.ifconfig.png)
17. gõ lệnh `vi /etc/network/interfaces` để đặt ip tĩnh cho ubuntu. Và điền thông tin như hình bên dưới:  
![interface](https://gitlab.com/thanhduongvs/beaglebone-black/raw/master/01.Board%20setup/image/04.interface.png)
18. Gõ lệnh `sudo /etc/init.d/networking restart` để resart lại.  
19. Gõ lệnh `sudo ifup  enp0s25` để up lại card mạng. Thế là cài đặt ip tĩnh cho ubuntu đã xong.  
20. Gõ lệnh `sudo apt-get install tftpd-hpa` để cài đặt giao tiếp *tftp*.  
21. Vào thư mục `cd /var/lib/tftpboot`  
22. Tạo file `sudo touch hello.txt`  
23. Ghi vào file `sudo vi hello.txt`  
24. Dùng tftp truyền qua local `sudo tftp 127.0.0.1`  
25. Truyền file hello `get hello.txt`  
26. Gõ phím `q` để thoát ra tftp.  
27. Bên trong uboot của beaglebone gõ lệnh `md 0x81000000`  
28. Gõ lệnh `tftp 0x81000000 hello.txt`  
29. Gõ lệnh `sudo apt-get install openssh-server` để cho máy khác có truy cập vào máy mình thông qua ssh.  
